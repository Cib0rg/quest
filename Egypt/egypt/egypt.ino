/* Скетч предназначен для реал-квеста "Машина времени"
 *  
 *  Данный скетч покрывает стартовую комнату и Лабораторию 1 (Древний Египет)
 * 
 *	Version 2.2
 *
 *	По большому количеству здравых размышлений понял, что старый метод решения проблемы "в лоб" некорректен.
 *	Решил сделать так: вместо мельтешения флагов я поделил последовательность квестовых действий на фазы.
 *	Фазы будут вызываться последовательно внутри setup(). Это гарантирует, что каждая фаза наступит только один раз.
 *	Внутри фазы будет что-то вроде while (!phaseCompleted) {} с обязательным гашением флага после конца фазы.
 *	После прохода всех-всех-всех фаз на все ардуины пройдёт команда Reset(), что вернёт их в стартовую конфигурацию.
 *	Внутри каждой фазы будет находиться блок чтения команд из UART для того, чтобы администратор мог вмешаться в любую из них, если дети попадут в трудное положение. 
 *
 */

//библиотека работы с реле
#include <Relay.h>

//============================================
/***ЗНАЧЕНИЯ ПИНОВ***/

/*
 * Блок реле будет разбит примерно так
 * 30 реле машины времени (магнитный замок)
 * 31 реле двери в египет + светодиод египта
 * 32 реле ящика с шестерёнкой
 * 33 реле входной двери + светодиод входа
 * 34 
 * 35 реле лазера
 * 36 реле дым-машины
 * 37 реле мотора внутри машины времени
 */

//пин реле мотора внутри МВ
 #define TIME_MACHINE_MOTOR_PIN 37

//пин реле включения дым-машины
#define SMOKE_RELAY_PIN 36

//пин реле, включающего лазерную указку
#define LASER_RELAY_PIN 35

//пин реле двери в Египт
#define EGYPT_DOOR_RELAY_PIN 31

//пин реле двери на выход
#define EXIT_DOOR_RELAY_PIN 33

//пин реле ящика с шестерёнкой
#define BOX_RELAY_PIN 32

//пин реле светодиодов над дверью в Египет
//#define EGYPT_DOOR_LIGHT_RELAY_PIN 31

//пин реле светодиодов над дверью выхода
#define TIME_MACHINE_RELAY_PIN 30

//пин реле, которое держит шестерёнку в квесте "Полярная звезда"
#define POLAR_STAR_RELAY_PIN 8

//пин кнопки "SOS"
//она же педаль
#define SOS_BUTTON_PIN 41

//пин кнопки "Запуск" в МВ
#define START_TM_BUTTON_PIN 43

//пин кнопки "Масло"
#define OIL_BUTTON_PIN A0

//пин кнопки "Пуск"
//она же запуск квеста
#define START_BUTTON_PIN 4

//пин микровыключателя на ящике с "Машиной времени"
#define TIME_MACHINE_BUTTON_PIN A11

//пин рычага на машине времени
#define TIME_MACHINE_ARM_PIN A8

//пин реле смены освещения
#define STROBO_RELAY_PIN 5

//датчик цвета
#define SENSOR_S0	8
#define SENSOR_S1	9
#define SENSOR_S2	11
#define SENSOR_S3	10
#define SENSOR_OUT	12
#define SENSOR_VCC	6

//=============================================

//пороговое значение от датчика цвета, выше которого будем считать, что попали

//примерный порог - 500-1000, текущий  только в дебаг-целях
#define SENSOR_OUT_MAX 500

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/

//флаг нажатия на кнопку "Пуск", т.е. начала квеста
volatile bool buttonStartPressedEvent = false;

//флаг открытия ящика "Машины времени"
volatile bool buttonTimeMachinePressedEvent = false;

//флаг нажатия кнопки "Послать сигнал"
volatile bool buttonSOSPressedEvent = false;

//флаг нажатия на кнопку "Масло"
volatile bool buttonOilPressedEvent = false;

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/

/*** ПЕРЕМЕННЫЕ ***/

String command = "";

//предопределённые переменные
char* raspberryStartMessage = "-play start movie";
char* raspberryErrorMessage = "-play error movie";
char* raspberryEndMessage = "-quest complete";

//аудиофайл "чихания без масла"
char* raspberrySnoozeCommand = "-play snooze audio";
//аудиофайл поломки машины
char* raspberryBrokenCommand = "-play broken audio";
//аудиофайл работы лазера
char* raspberryLaserStartCommand = "-play laser start sound";
//аудиофайл открытия двери
char* raspberryOpenDoorSoundCommand = "-play open door sound";
//аудиофайл "Сирены"
char* raspberryAlarmSoundCommand = "-play alarm sound";
//аудиофайл "Сообщение в звезду отправлено"
char* raspberryLaserStopSoundCommand = "-play laser stop sound";
//команда на отключение звука
char* raspberryStopSoundCommand = "-stop audio";
//команда на отключение видео
char* raspberryStopVideoCommand = "-stop video";
//аудио
//команда на соседа о том, что код на двери уже можно принимать
char* raspberryGreeceCodeAllowCommand = "-greece code allow";

//флаг окончания квеста
bool allDoneFlag = false;

/*** ПЕРЕМЕННЫЕ ***/

//на имеющемся шилде логика реле инвертирована
//по умолчанию оно ВКЛЮЧЕНО, чтобы его выключить - надо подать на него 5 вольт
//изменяем логику

//реле, контролирующее доступ к следующей комнате
//открыто без питания, в начале квеста закрыто
Relay egyptDoorRelay(EGYPT_DOOR_RELAY_PIN,ON, true);

//реле проблескового маяка
//открыто без питания, в начале квеста открыто
//отдельное реле, не шилдовое! не трогать!
Relay stroboRelay(STROBO_RELAY_PIN,false);

//реле диодов над дверью с Египет
//открыто без питания, в начале квеста открыто
//Relay egyptLightRelay(EGYPT_DOOR_LIGHT_RELAY_PIN,OFF, true);

//реле диодов внутри МВ
Relay timeMachineRelay(TIME_MACHINE_RELAY_PIN, OFF, true);

//реле, контролирующее доступ к выходу
//открыто без питания, в начале квеста закрыто
Relay exitDoorRelay(EXIT_DOOR_RELAY_PIN,ON, true);

//реле, управляющее ящиком с конвертом
//без питания должно быть закрыто, так что надо размыкать
Relay boxRelay(BOX_RELAY_PIN,OFF, true);

//реле, управляющее дым-машиной
//включается только по сигналу, так что без питания не замыкает
Relay smokeRelay(SMOKE_RELAY_PIN,OFF, true);

//реле, включающее лазерную пушку
Relay laserRelay(LASER_RELAY_PIN,OFF, true);

//реле мотора внутри МВ
Relay engineRelay(TIME_MACHINE_MOTOR_PIN, OFF, true);

//обьявляем функцию для программного сброса Ардуины (на всякий случай)
void(* Reset) (void) = 0;//declare reset function at address 0

void setup() {
	
	//======================================================
	/***	ПЕРВОНАЧАЛЬНАЯ НАСТРОЙКА ПИНОВ СИСТЕМЫ И ПРЕРЫВАНИЙ	***/

  //с прерываниями ничего не вышло (ебучая Mega), придётся колхозить флагами и считыванием
  //со входа стандартным digitalRead() в цикле
  //а счастье было так близко...
	
  //инициализируем UART, через него мы получим информацию о том, что дверь можно открыть
  Serial.begin(9600);

  //выставляем порты
  //начнём с кнопки "Послать сигнал"
  pinMode(SOS_BUTTON_PIN, INPUT);
  //включаем внутренний подтягивающий резистор
  digitalWrite(SOS_BUTTON_PIN, HIGH);
  
  //настраиваем кнопку "Пуск"
  pinMode(START_BUTTON_PIN, INPUT);
  digitalWrite(START_BUTTON_PIN, HIGH);

  //настраиваем микровыключатель в "Машине времени"
  pinMode(TIME_MACHINE_BUTTON_PIN, INPUT);
  digitalWrite(TIME_MACHINE_BUTTON_PIN, HIGH);

  //настраиваем пин рычага в МВ
  pinMode(TIME_MACHINE_ARM_PIN, INPUT);
  digitalWrite(TIME_MACHINE_ARM_PIN,HIGH);

  //настраиваем пин кнопки "Запуск" в МВ
  pinMode(START_TM_BUTTON_PIN, INPUT);
  digitalWrite(START_TM_BUTTON_PIN, HIGH);


  pinMode(OIL_BUTTON_PIN, INPUT);
  digitalWrite(OIL_BUTTON_PIN, HIGH);
  
  //настройка датчика цвета
  //выставляем пины в нужное положение
  pinMode(SENSOR_S0,OUTPUT);
  pinMode(SENSOR_S1,OUTPUT); 
  pinMode(SENSOR_S2,OUTPUT);
  pinMode(SENSOR_S3,OUTPUT);
  pinMode(SENSOR_OUT, INPUT);
 
  //выбираем зелёный светофильтр
  digitalWrite(SENSOR_S2, HIGH);
  digitalWrite(SENSOR_S3, HIGH);
  
  //пока что сенсор выключен, включим, когда будет надо
  digitalWrite(SENSOR_S0, LOW);
  digitalWrite(SENSOR_S1, LOW);
 
  /***	КОНЕЦ ПРЕДВАРИТЕЛЬНОЙ НАСТРОЙКИ СИСТЕМЫ	***/

  //очищаем буфер от мусора
  while (Serial.available() > 0) {
                // read the incoming byte:
                int incomingByte = Serial.read();
  }

  /***	ПООЧЕРЁДНЫЙ ЗАПУСК ФАЗ КВЕСТА	***/

  Phase0();
  Phase1();
  Phase2();
  Phase3();
  Phase4();
  
  /***	КОНЕЦ ФАЗ КВЕСТА	***/
  
}

/***	КОД ФАЗ КВЕСТА	***/

/* Фаза 0
 * Нанокостыль для подгонки логики под уже отснятое видео
 * Пока рычаг на МВ опущен - квест не начинается. Как только его подняли - запускается фаза
 * После небольшой задержки запускается стартовое видео и дальше уже по сценарию.
 */
void Phase0()
{
   bool phaseCompleted = false;

  //открываем дверь выхода
  exitDoorRelay.Off();

   while (!phaseCompleted)
   {
    serialEvent();
      //проверяем состояние рычага
      if ( digitalRead(TIME_MACHINE_ARM_PIN) == HIGH) 
      {
          phaseCompleted = true;

          //задержка
          delay(6000);

          //закрываем дверь выхода
          exitDoorRelay.On();

          //запускаем видео
          SendToRaspberry(raspberryStartMessage);
      }

  if ( command != "" )
  {
    ParceInput();
  }
   }
}

/* Фаза 1.
 * Начинается с нажатия кнопки "Пуск", заканчивается командой на Малину о запуске вступительного ролика
*/
void Phase1()
{
	//флаг конца фазы
	bool phaseCompleted = false;

  bool prev = true;
	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();
		//пока не нажата кнопка "Пуск" - делаем ничего
    //одна-единственная с(т)ран(н)ая кнопка, которая работает не так, как остальные, а на размыкание
    //кнопка, выходит, нахрен не нужна?
		//if (digitalRead(START_BUTTON_PIN) == HIGH && prev == true)
		{
    
      prev = false;
			
			phaseCompleted = true;

      //делим задержки на условные и безусловные
      //безусловная 40 секунд + условная 32 секунды
      for (int i = 0; i< 72; i++)
      {
        delay(1000);
      }
      //примерно в это время на первом видео происходит авария

      //тут у нас АВАРИЯ

      //включаем проблесковый маячок
      stroboRelay.On();

      //SendToRaspberry(raspberryBrokenCommand);

      smokeRelay.On();
      //delay(5000);

      //начинаем играть сирену
      SendToRaspberry(raspberryAlarmSoundCommand);
      
      for (int i = 0; i < 7; i++)
      {
      //включаем дым-машину
      smokeRelay.On();
      delay(2000);
      smokeRelay.Off();
      delay(1000);
      }



		}

   
		
		//мониторинг UART ардуины в течение фазы
		//если на вход что-то поступило - обрабатываем
		if ( command != "" )
		{
			ParceInput();
		}
	}
}

/* Фаза 2.
 * Начинается с нажатия рычага открытия "Машины времени" (точнее, с открытия крышки - на ящике стоит микровыключатель, который размыкается).
 * Затем сверху падает конверт.
 * Затем открывается дверь в Египет.
 * Затем над дверью в Египет загорается зелёный светодиод.
 */
void Phase2()
{
	//флаг конца фазы
	bool phaseCompleted = false;
	
	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();
		//пока не открыт ящик "Машины времени" - ничего не делаем
    if (digitalRead(TIME_MACHINE_ARM_PIN) == LOW)
		{
      //Отключаем стробоскоп
      stroboRelay.Off();
      
       //стопим видео
      SendToRaspberry(raspberryStopVideoCommand);

      //открываем МВ
      timeMachineRelay.On();
      delay(1000);
      //играем второе видео
      SendToRaspberry(raspberryErrorMessage);
      //останавливаем сирену
      SendToRaspberry(raspberryStopSoundCommand);
			//ждём середины видео
      for (int i =0; i< 58; i++) delay (1000);
      
      //delay(5000);
       SendToRaspberry(raspberryStopVideoCommand);

      //играем звук открытия двери
      SendToRaspberry(raspberryOpenDoorSoundCommand);
      //открываем дверь в Египет
      egyptDoorRelay.Off();
      
			//фаза завершена
			phaseCompleted = true;
		}
		
		//мониторинг UART ардуины в течение фазы
		//если на вход что-то поступило - обрабатываем
		if ( command != "" )
		{
			ParceInput();
		}
	}
}

/* Фаза 3.
 * Включает в себя задание "Полярная звезда".
 * Начинается с нажатия кнопки "Послать сигнал".
 * Заканчивается отключением реле, удерживающим шестерёнку задания "Полярная звезда".
 */
void Phase3()
{
	//флаг конца фазы
	bool phaseCompleted = false;

  long cTime = millis();
	
	//инициализация датчика цвета
  ColorSensorStart();

  //флаг того, что лазер включен
  bool laserOn = false;
  
  //laserRelay.Switch();

	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();

    /*
     * Изменения в сценарии
     * Лазер включается при нажатии на педаль
     */

     if (!laserOn)
     {
        //если педаль нажата - выставляем флаг и едем дальше)
        if (digitalRead(SOS_BUTTON_PIN) == LOW) 
        {
          laserOn = true;
            //запускаем звук лазера
          SendToRaspberry(raspberryLaserStartCommand);
        }
     }
      else
      {
        //пытаемся реализовать мигание
        if (millis() - cTime > 500 )
        {
          laserRelay.Switch();
          cTime = millis();
        }
		    //если нажата кнопка "Послать сигнал"
        //if (digitalRead(SOS_BUTTON_PIN) == LOW)
		    {
			    //включаем датчик цвета

          int intence = GetBeamIntencity();
			    //если дети попали в датчик
			    if (intence > SENSOR_OUT_MAX)
			    {
				    //отдаём шестерёнку
				    boxRelay.On();
            //останавливаем текущее аудио
            SendToRaspberry(raspberryStopSoundCommand);

            //играем аудио "победы"
            SendToRaspberry(raspberryLaserStopSoundCommand);
				    //отключаем датчик цвета

				    //завершаем фазу
				    phaseCompleted = true;
			    }
			  //если же нет - сбрасываем флаг прерывания
		    }
      }
		//мониторинг UART ардуины в течение фазы
		//если на вход что-то поступило - обрабатываем
		if ( command != "" )
		{
			ParceInput();
		}
	}
 
 //по завершению фазы выключаем дачик цвета, он не нужен больше
 ColorSensorStop();
 //да и лазер вырубим
 laserRelay.Off();

 //посылаем в Грецию сообщение о том, что Звезда пройдена
 //и можно принимать код с дыери
 SendToRaspberry(raspberryGreeceCodeAllowCommand);
}

//дополнительный блок кода для работы с датчиком цвета
//стартуем датчик цвета
void ColorSensorStart()
{
  digitalWrite(SENSOR_S0, LOW);  // OUTPUT FREQUENCY SCALING 2%
  digitalWrite(SENSOR_S1, HIGH);
  
  //включаем датчик и ждём, пока он прогреется
  digitalWrite(SENSOR_VCC, HIGH);
  delay(100);
}

//отключаем датчик цвета
void ColorSensorStop()
{
	digitalWrite(SENSOR_VCC, LOW);
}

//вытаскиваем информацию о цвете лазера
int GetBeamIntencity()
{
  int count = 0;
	int lastDigitalRead = HIGH;
    for(int j=0; j<20000; j++)
    {
      int digitalReadValue = digitalRead(SENSOR_OUT);
      if (lastDigitalRead == LOW && digitalReadValue == HIGH) 
      {
        count++;
      }
      lastDigitalRead = digitalReadValue;
    }

	return count;
}

/* Фаза 4
 * Начинается после конца 3 фазы, активной становится по приходу флага "all done"
 * Данный флаг означает, что у детей есть все шестерёнки и что квест пройден почти весь
 */
void Phase4()
{
	//флаг окончания фазы
	bool phaseCompleted = false;

   bool oilFlag = false;

	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();

            
        //мониторинг UART ардуины в течение фазы
        //если на вход что-то поступило - обрабатываем
        if ( command != "" )
        {
          ParceInput();
        }
		//если выставлен флаг all_done - то делаем хоть что-то
		if (allDoneFlag)
		{

      /*
       * Примерная логика:
       * сюда мы попадаем, когда уже всё готово
       * тут при нажатии на кнопку "Пуск" раздаётся чих, если предварительно не была нажата кнопка "Масло"
       * в таком случае играется аудиофайл с чихом и видео с советующим учёным
       * если порядок не был нарушен, то играются фанфары, запускается мотор внутре и открываются двери выхода
       */
      //неверный порядок операций
      if (digitalRead(START_TM_BUTTON_PIN) == HIGH && digitalRead(OIL_BUTTON_PIN) == HIGH)
      {
        //стопим звук
        SendToRaspberry(raspberryStopSoundCommand);
        //проигрываем "чихание"
        SendToRaspberry(raspberrySnoozeCommand);

        //ждём, пока проиграется
        delay(1000);
        oilFlag = true;
      }

      if (digitalRead(START_TM_BUTTON_PIN) == HIGH && digitalRead(OIL_BUTTON_PIN) == LOW && oilFlag)
      {
        SendToRaspberry(raspberryStopSoundCommand);
        //всё закончилось
        engineRelay.On();

        //слегка играемся
        //delay(3000);

        //проигрываем поздравления
        SendToRaspberry(raspberryEndMessage);

        //ждём
        delay(6000);

        //перестаём крутить замок
        engineRelay.Off();
        
        //открываем дверь
        exitDoorRelay.Off();

        phaseCompleted = true;
      }
      
		}
  }
}


/***	КОНЕЦ КОДА ФАЗ КВЕСТА	***/

void loop() {
	//мониторинг UART ардуины в течение фазы
	//если на вход что-то поступило - обрабатываем
	if ( command != "" )
	{
		ParceInput();
	}
}

/*** КОМАНДЫ УПРАВЛЕНИЯ ***/

/*** КОМАНДЫ УПРАВЛЕНИЯ ***/

//функция, вызывающаяся тогда, когда в порту появляются данные
//так как тут у нас нет жёстких таймингов, управлять открытием-закрытием мы будем вручную, через Serial-команды
void serialEvent()
{
  int t = Serial.read();

  //на всякий случай - очистка буфера команд по знаку @
  if (t == 95) command = "";
  else
  if (t != -1 && t!= 13 && t != 10)  
  {
    command += (char) t;
    //Serial.println(command);
  }
}

//отправляем команду на малину
void SendToRaspberry(char* message)
{
  Serial.println(message);
}

//функция парсинга входящих команд (UART)
void ParceInput()
{
    if ( command == "all done" )
    {
      allDoneFlag = true;
      command = "";
      return;
    }

	//команда перезагрузки ардуины. Пользоваться с осторожностью!
	if ( command == "reset" )
    {
      //Serial.println("Resetting");
      
      command = "";
	    Reset();
      return;
    }

  /*
   * Секция аварийных команд, которые позволяют просто "перещёлкнуть" нужное реле
   */
   //дверь Египта
    if ( command == "egypt door" )
    {
      command = "";
     egyptDoorRelay.Switch();
      return;
    }

    //стробоскоп
    if ( command == "strobo" )
    {
      command = "";
     stroboRelay.Switch();
      return;
    }

    //дверь выхода
    if ( command == "exit door" )
    {
      command = "";
     exitDoorRelay.Switch();
      return;
    }

    //ящик с шестерёнкой
    if ( command == "box" )
    {
      command = "";
     boxRelay.Switch();
      return;
    }

    //дым-машина
    if ( command == "smoke" )
    {
      command = "";
     smokeRelay.Switch();
      return;
    }

    //лазер
    if ( command == "laser" )
    {
      command = "";
     laserRelay.Switch();
      return;
    }

    //двигатель в машине времени
    if ( command == "engine" )
    {
      command = "";
     engineRelay.Switch();
      return;
    }
	
	if ( command == "time machine" )
    {
      command = "";
     timeMachineRelay.Switch();
      return;
    }
}

/***	ОБРАБОТЧИКИ ПРЕРЫВАНИЙ	***/

/***	КОНЕЦ ОБРАБОТЧИКОВ ПРЕРЫВАНИЙ	***/
