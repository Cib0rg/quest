﻿using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
namespace SerialServer
{
    class Serial
    {
        SerialPort port;

                public Serial(string portName, int baudRate, int portNumber)
        {
            port = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
            port.DtrEnable = false;
            port.Open();

            //стартуем листенер по указанному порту

        }

        //делаем событие
        public delegate void OnCommandRecieved();

        public event OnCommandRecieved NewCommand;

        public void StartListening()
        {
            while (true)
            {
                Thread.Sleep(200);
                if (port.BytesToRead > 0)
                {
                    Program.logger.Log("There is bytes");
                    string s = port.ReadLine().Trim();

                    Program.logger.Log("Bytes no more");
                    Program.logger.Log(s);

                    //пока что Enqueue
                    Program.commandsFromCom.Enqueue(s);
                    Program.logger.Log("Enqueued" + s);

                    //а вообще - надо так
                    //теперь, когда мы получаем новое сообщение, происходит событие, которое мы можем обработать из
                    //основного потока программы. Там хранится словарь обработки. Что логично - нехрен клиенту для работы 
                    // с СОМ-портом знать, для чего он это делает.
                    NewCommand();
                }
            }
        }

        public void SendCommand(string command)
        {
            port.WriteLine(command);
        }

        ~Serial()
        {
            port.Close();
        }
    }
}
