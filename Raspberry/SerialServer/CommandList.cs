﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Linq;
using System.Text;

namespace SerialServer
{
    class CommandList
    {
        private List<Command> commands = new List<Command>();

        public CommandList(string path)
        {
            if (File.Exists(path))
            {
                XDocument doc = XDocument.Parse(System.IO.File.ReadAllText(path));

                IEnumerable<XElement> roots = doc.Descendants().Where(x => x.Name == "Command");

                foreach (XElement item in roots)
                {
                    Command command = new Command();
                    command.arduinoCommand = item.Descendants().FirstOrDefault(x => x.Name == "arduino").Value;
                    command.shellCommand = item.Descendants().FirstOrDefault(x => x.Name == "shell").Value;
                    command.arguments = item.Descendants().FirstOrDefault(x => x.Name == "arguments").Value;
                    commands.Add(command);
                }

            }
            
        }

        public ShellCommand GetCommand(string arduinoCommand)
        {
            ShellCommand scm = new ShellCommand();

            Command t = commands.FirstOrDefault(x => x.arduinoCommand == arduinoCommand);

            scm.command = t.shellCommand;
            scm.arguments = t.arguments;

            return scm;
        }

        public int CommandCount()
        {
            return commands.Count;
        }
        
    }

    class ShellCommand
    {
        public string command;
        public string arguments;
    }

    class Command
    {
        public string arduinoCommand;
        public string shellCommand;
        public string arguments;
    }

    class ButtonList
    {
        Dictionary<string, string> buttons = new Dictionary<string, string>();

        private string _hostname = "";

        public string Hostname
        {
            get { return _hostname; }
            set { _hostname = value; }
        }

        private string _port = "";

        public string Port
        {
            get { return _port; }
            set { _port = value; }
        }

        public int GetCount()
        {
            return buttons.Count;
        }

        public ButtonList(string path)
        {
            if (File.Exists(path))
            {
                XDocument doc = XDocument.Parse(System.IO.File.ReadAllText(path, Encoding.UTF8));

                IEnumerable<XElement> roots = doc.Descendants().Where(x => x.Name == "Button");

                foreach (XElement item in roots)
                {
                    string buttonText = String.Empty;
                    string buttonAction = String.Empty;

                    buttonText = item.Descendants().FirstOrDefault(x => x.Name == "ButtonText").Value;
                    buttonAction = item.Descendants().FirstOrDefault(x => x.Name == "ButtonAction").Value;
                    buttons.Add(buttonAction, buttonText);
                }

            }
            
        }

        public string GetPage()
        {
            string page = String.Format("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\"http://www.w3.org/TR/html4/loose.dtd\"><html><head><meta http-equiv=\"Content-Type\"content=\"text/html; charset=iso-8859-1\"><title>{0}</title></head><body>", _hostname);

            //генерация основного кода страницы
            foreach (var button in buttons)
            {
                page += this.MakeHTMLButton(_hostname, _port, button.Key, button.Value);
            }

            page += "</body></html>";
            return page;
        }

        public string MakeHTMLButton(string action, string text)
        {
            return this.MakeHTMLButton(_hostname, _port, action, text);
        }

        public string MakeHTMLButton(string myHostname, string port, string action, string text)
        {
            string rawButton = "<a href=\"http://{0}:{1}/?action={2}\"><button>{3}</button></a>";
            return String.Format(rawButton, new object[] { myHostname, port, action, text });
        }
    }
}
