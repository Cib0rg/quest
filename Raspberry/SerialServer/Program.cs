﻿using System;
using System.Threading;
using WebServer;
using System.Net;
using System.Diagnostics;
using System.Collections.Generic;

namespace SerialServer
{
    class Program
    {
        const int ARGS_COUNT = 2;

        public static CommandList commands = new CommandList("commands.txt");
        public static ButtonList buttons = new ButtonList("buttons.txt");
        public static Logger logger = new Logger("log.txt");
        public static string ACTION_REGEX = @"^/\?action=(.+)$";

        public static Queue<string> commandsFromWeb = new Queue<string>();
        public static Queue<string> commandsFromCom = new Queue<string>();

        static void Main(string[] args)
        {

            string portName = "/dev/ttyACM0";
            //string portName = "COM7";
            int baudRate = 9600;
            int portNumber = 9600;

            string serverName = Dns.GetHostName();

           // Logger logger = new Logger("log.txt");

            //загружаем список команд из файла в память.
            //это нужно для дальнейшей корректной интерпритации того, что ардуино будет нам присылать
            //например, туда можно засунуть список файлов для проигрывания и дёргать их по замкнутой-разомкннутой цепи
            //CommandList commands = new CommandList(pathToCommandFile);

            if (commands != null && commands.CommandCount() > 0)
                logger.Log("Parced file with commands");
            else logger.Log("Commands not loaded or zero commands loaded!");

            //ButtonList buttons = new ButtonList(pathToButtonFile);

            if (buttons != null && buttons.GetCount() > 0)
                logger.Log("Parced file with buttons");
            else logger.Log("Buttons not loaded or zero buttons loaded!");

            buttons.Port = Convert.ToString(portNumber); ;
            buttons.Hostname = serverName;

           
            /*
             
             string portname = args[0];
             int baudRate = Convert.ToInt32(args[1]);
            if (args.Length != ARGS_COUNT)
            {
                Console.WriteLine(Usage());
                return;
            }
             
             */
            Serial serialServer = new Serial(portName, baudRate, portNumber);

            //подписываемся на событие "Получено новое сообщение от ардуино"
            serialServer.NewCommand += delegate
            {
				try
                {
                logger.Log("Serial delegate entered");
                var rawCommand = commandsFromCom.Dequeue();
				
				/*	Команда может прийти с мусором в начале
				 *	Для борьбы с этим делаем следующее: резервируем символ '_' как служебный символ очистки буфера
				 *	Это означает, что мы можем просто делать Split('_') и брать последний элемент массива
				 */
				var cleanCommandArray = rawCommand.Split('-');
				var cleanCommand = cleanCommandArray[cleanCommandArray.Length - 1];
				
                var s = commands.GetCommand(cleanCommand);
                logger.Log("Get new command from Arduino: " + cleanCommand);
					
                    RunInShell(s);
                }
                catch (Exception ex)
                {
                    logger.Log("Exception on executing command: " + ex.Message);
                    logger.Log("Full stacktrace: " + ex.StackTrace);
                }

            };

            Thread thread = new Thread(new ThreadStart(serialServer.StartListening));
            thread.Start();

            //запускаем веб-сервер
            MyHttpServer httpServer;
            if (args.GetLength(0) > 0)
            {
                httpServer = new MyHttpServer(Convert.ToInt16(args[0]));
            }
            else
            {
                httpServer = new MyHttpServer(portNumber);
            }

            //событие: пришло новое сообщение из веб-канала
            httpServer.NewCommand += delegate
            {
                logger.Log("New command from web received");
                string command = commandsFromWeb.Dequeue();

                //если команда начинается с символов com_ - то она предназначена для отправки на ардуину
                if (command.StartsWith("com_"))
                {
                    //удаляем этот самый com_
                    string trimmedCommand = command.Remove(0, 4).Trim();

                    serialServer.SendCommand(trimmedCommand);
                }
                //команда не для ардуины, а для самой малины
                else
                {
                    logger.Log(command);

                    ShellCommand s = commands.GetCommand(command);
                    RunInShell(s);
                }
            };

            Thread serialThread = new Thread(new ThreadStart(httpServer.listen));
            serialThread.Start();
            
        }

        private static void RunInShell(ShellCommand s)
        {
            Process ps = new Process();

            Program.logger.Log("command " + s.command + " " + s.arguments);

            ps.StartInfo = new ProcessStartInfo(s.command, s.arguments);
            ps.Start();
        }

        static string Usage()
        {

            string usage = "Error! Usage: mono SerialServer.exe <port name> <baud rate> <listener port>";
            return usage;
        }
    }

}
