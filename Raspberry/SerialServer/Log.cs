﻿using System;
using System.Text;
using System.IO;

namespace SerialServer
{
    class Logger
    {
        private StreamWriter sw;

        public Logger(string fileName)
        {
            sw = new StreamWriter(fileName, true, Encoding.GetEncoding("UTF-8"));
            sw.AutoFlush = true;
        }

        public void Log(string message)
        {
            sw.WriteLine(DateTime.Now.ToString("HH:mm::ss") + " " + message);
            Console.WriteLine(DateTime.Now.ToString("HH:mm::ss") + " " + message);
        }

    }
}
