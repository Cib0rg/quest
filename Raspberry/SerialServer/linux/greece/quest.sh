#!/bin/bash

cd /home/pi/code

#export DISPLAY=:0.0
xhost +

env > /home/pi/env.txt

nohup mono CommandList.exe >& /dev/null &
