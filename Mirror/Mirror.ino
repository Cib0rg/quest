//библиотека для работы с цифровой клавиатурой
#include <Keypad.h>

#define MIRROR_PIN A0

//пин пищалки
#define BUZZER_PIN 3

/*************************************************
 * Public Constants
 *************************************************/
 
#define NOTE_C4  262
#define NOTE_C7  2093

//Mario main theme melody
int melodyCorrect = NOTE_C7;
//Mario main them tempo
int tempoCorrect = 6;

int melodyWrong = NOTE_C4;

int tempoWrong = 2;

/*  В связи с внезапной необходимостью читать весь ввод с клавиатуры
 *  и отсутствием поддержки кириллицы ардуиной и терминалом малины
 *  часть символов будет кодироваться другими
 *  Ашопаделать?
 *  
 *  Список замен (11 символов)
 *  ж - #
 *  й - 1
 *  ч - 3
 *  ш - 4
 *  щ - 5
 *  ъ - 6
 *  ь - 8
 *  э - 9
 *  ю - 0
 *  я - *
 */

//цифровая клавиатура
const byte ROWS = 4; 
const byte COLS = 4; 
char keys1[ROWS][COLS] = {
  {'l','*','4','k'},      //
  {'v','a','6','n'},      //
  {'u','3','g','b'},      //
  {'0','z','9','f'}       //
};

char keys2[ROWS][COLS] = {
  {'5','c','y','#'},
  {'h','8','e','p'},
  {'1','s','d','m'},
  {'r','i','t','o'}
};

byte rowPins1[ROWS] = {22,23,24,25}; //connect to row pinouts 
byte colPins1[COLS] = {26,27,28,29}; //connect to column pinouts

byte rowPins2[ROWS] = {40,41,42,43}; //connect to row pinouts 
byte colPins2[COLS] = {44,45,46,47}; //connect to column pinouts

Keypad keypad1 = Keypad( makeKeymap(keys1), rowPins1, colPins1, ROWS, COLS );
Keypad keypad2 = Keypad( makeKeymap(keys2), rowPins2, colPins2, ROWS, COLS );

//количество цифр в коде
const unsigned short codeLength = 7;
//код для открытия двери в Древнюю Грецию
char correctCode[codeLength] = { 'z','e','r','k','a','l','o'};

//указатель на текущую букву кода
int codePointer = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  //связь с другой ардуиной
  Serial1.begin(9600);

  pinMode(MIRROR_PIN, OUTPUT);
  digitalWrite(MIRROR_PIN, LOW);

  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}

//проверка ввода корректного кода для открытия двери в следующую комнату (Древняя Греция)
bool EnterCode(char key)
{
  bool result = false;
    //проверяем, совпадает ли нажатая клавиша с ожидаемой
    if (key == correctCode[codePointer])
    {
      codePointer++;

     //отправка соседу всего набора нажатых клавиш
     Serial1.print("_");
     for (int i=0; i < codePointer; i++)
     {
        Serial1.print(correctCode[i]);
     }
     //отправляем \n
     Serial1.print('\n');

      play(true);
      
      //введён ли код до конца?
      if (codePointer >= codeLength)
      {
    result = true;
      }
    }
    //дети где-то ошиблись
    else 
    {
      //отправка на соседа кода кнопки
      Serial1.print("_");
      Serial1.print(key);
      Serial1.print('\n');
      
      //сбрасываем указатель на начало кода
      codePointer = 0;
      play(false);
    }
  return result;
}

void loop() {
  // put your main code here, to run repeatedly:
char key1 = keypad1.getKey();
char key2 = keypad2.getKey();

    if (key1 != NO_KEY || key2 != NO_KEY){
      //если ввели корректный код - то вернётся true
      //DEBUG
      if (key1 != NO_KEY) Serial.println(key1);
      if (key2 != NO_KEY) Serial.println(key2);
      
      char key = '1';

      if (key1 == NO_KEY) key = key2;
      else key = key1;
      
      if ( EnterCode(key) )
      {
        Serial.println("Done");
        digitalWrite(MIRROR_PIN, HIGH);
        digitalWrite(13, HIGH);
        delay(1000);
        digitalWrite(MIRROR_PIN, LOW);
        digitalWrite(13, LOW);
        codePointer = 0;
      }
    }
}

void play(bool correct)
{
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      if (!correct)
      {
      int noteDuration = 1000 / tempoWrong;
 
      buzz(melodyWrong, noteDuration);
 
      // to distinguish the notes, set a minimum time between them.
      // the note's duration + 30% seems to work well:
      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);
 
      // stop the tone playing:
      buzz(0, noteDuration/2);
      }

      //если последовательность продолжается
      else
      {
      int noteDuration = 1000 / tempoCorrect;
 
      buzz(melodyCorrect, noteDuration);
 
      // to distinguish the notes, set a minimum time between them.
      // the note's duration + 30% seems to work well:
      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);
 
      // stop the tone playing:
      buzz(0, noteDuration);
      }
 
}

//функция-пищалка
void buzz(long frequency, long length) {
  long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
  //// 1 second's worth of microseconds, divided by the frequency, then split in half since
  //// there are two phases to each cycle
  long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
  //// multiply frequency, which is really cycles per second, by the number of seconds to
  //// get the total number of cycles to produce
  for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
    digitalWrite(BUZZER_PIN, HIGH); // write the buzzer pin high to push out the diaphram
    delayMicroseconds(delayValue); // wait for the calculated delay value
    digitalWrite(BUZZER_PIN, LOW); // write the buzzer pin low to pull back the diaphram
    delayMicroseconds(delayValue); // wait again or the calculated delay value
  } 
}
