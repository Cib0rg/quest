// Relay.h

#ifndef _RELAY_h
#define _RELAY_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#define ON true
#define OFF false

class Relay
{
 private:
	int relayPin;
	bool relayStatus;
	
	//��������� �������� ����� ������� ����� ���� � ������ ���������� ��� ������ �� ��� ����
	//��� ���� ����� ������, ����� �� ��� �� ����� ��������� �������� ������ On � Off, ��� �������� ����
	bool inverted;

 public:
	//�����������
	//pin - ���, � �������� ��������� ����������� ����� ����
	//status - true/false, ��������/���������
	Relay(int pinConnected, bool isOn);
	Relay(int pinConnected);

	//��� ����������� ��� ��������������� ����
	Relay(int pinConnected, bool isOn, bool isInverted);
	
	//�������� ������� ����
	bool IsOn();
	
	//��������� ������� ����
	void On();
	void Off();
	void Switch();
	
};



#endif

