// 
// 
// 

#include "Relay.h"

	//�����������
	//pin - ���, � �������� ��������� ����������� ����� ����
	//status - true/false, ��������/���������
	Relay::Relay(int pinConnected, bool isOn)
	{
		relayPin = pinConnected;
		pinMode(relayPin, OUTPUT);
		
		inverted = false;
		
		if (isOn)
		{
			On();
		}
		else 
		{
			Off();
		}
	};
	
	//�� ��������� - ���������
	Relay::Relay(int pinConnected)
	{
		relayPin = pinConnected;
		pinMode(relayPin, OUTPUT);
		
		inverted = false;
		
		this->Off();
	};
	
	Relay::Relay(int pinConnected, bool isOn, bool isInverted)
	{
		relayPin = pinConnected;
		pinMode(relayPin, OUTPUT);
		
		inverted = isInverted;
		
		if (isOn)
		{
			On();
		}
		else 
		{
			Off();
		}
	}
	
	//�������� ������� ����
	bool Relay::IsOn()
	{
		return relayStatus;
	};
	
	//��������� ������� ����
	void Relay::On()
	{
		if (!inverted)
			digitalWrite(relayPin, HIGH);
		else digitalWrite(relayPin, LOW);
		relayStatus = true;
	};
	
	void Relay::Off()
	{
		if (!inverted)
			digitalWrite(relayPin, LOW);
		else digitalWrite(relayPin, HIGH);
		relayStatus = false;
	};
	
	void Relay::Switch()
	{
		if (relayStatus) this->Off();
		else this->On();
	};




