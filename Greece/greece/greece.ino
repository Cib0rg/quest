/* Скетч предназначен для реал-квеста "Машина времени"
 *  
 *  Данный скетч покрывает Лабораторию 2 (Древний Египет)
 * 
 *	Version 2.1
 *
 *	По большому количеству здравых размышлений понял, что старый метод решения проблемы "в лоб" некорректен.
 *	Решил сделать так: вместо мельтешения флагов я поделил последовательность квестовых действий на фазы.
 *	Фазы будут вызываться последовательно внутри setup(). Это гарантирует, что каждая фаза наступит только один раз.
 *	Внутри фазы будет что-то вроде while (!phaseCompleted) {} с обязательным гашением флага после конца фазы.
 *	После прохода всех-всех-всех фаз на все ардуины пройдёт команда Reset(), что вернёт их в стартовую конфигурацию.
 *	Внутри каждой фазы будет находиться блок чтения команд из UART для того, чтобы администратор мог вмешаться в любую из них, если дети попадут в трудное положение. 
 *
 */

//библиотека работы с реле
#include <Relay.h>

//библиотека для работы с сервоприводами
#include <Servo.h> 

//библиотека для работы с цифровой клавиатурой
#include <Keypad.h>

/***ЗНАЧЕНИЯ ПИНОВ***/

//блок реле
/*
 * 1 светодиоды двери + электрозамок на двери в Грецию
 * 2 магнит, удерживающий колесницу
 * 3 реле сигнала от "Зеркала"
 * 4 подсветка лабиринта (светодиодная лента)
 */

//пин реле светодиодов на двери
#define GREECE_DOOR_RELAY_PIN 30
//пин реле колесницы Гелиоса
#define WHEELCHAIR_RELAY_PIN 31
//реле включения подсветки лабиринта
#define MAZE_RELAY_PIN 33

//постройка храма
#define TEMPLE_PIN A0
//пин микровыключателя в конце лабиринта
#define MAZE_BUTTON_PIN A1
//ответ от "зеркала"
#define MIRROR_PIN 12
//четыре одновременных кнопки
#define FOUR_BUTTONS_PIN 10

//ШИМ-лапка, к которой подключен сервопривод
//к которому примотана планка, удерживающая шарик в лабиринте
//на стартовой позиции
#define SERVO_PIN 11

//пин датчика движения
#define MOTION_SENSOR_PIN A4


//пин встреонного в плату красного светодиода
#define ledPin 13

//===============================================

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/
//флаг срабатывания события "Колесница Гелиоса" (доехали до конца и замкнули кнопку)
volatile bool flagWheelchairEvent = false;

//флаг срабатывания события "Сборка храма"
volatile bool buttonTempleConstructionEvent = false;

//флаг срабатывания события "Введён правильный код "Зеркала""
//в таком виде потому, что клавиатуру и экран будет обрабатывать отдельная ардуина
//и она просто выдаст HIGH в какой-нибудь порт после того, как код будет введёт верно
volatile bool buttonMirrorEvent = false;

//основной флаг: флаг старта всей работы. Физически - сигнал от датчика присутствия
volatile bool motionSensorEvent = false;

//флаг нажатия на кнопку "Помощь"
volatile bool buttonHelpPressedEvent = false;

//флаг срабатывания микровыключателя в конце лабиринта
volatile bool buttonMazePressedEvent = false;

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/

/*** ПЕРЕМЕННЫЕ ***/

String command = "";
String command1 = "";

//команды для малины
char* raspberryOpenRomeDoorCommand = "-open rome door";
char* raspberryMirrorMovieCommand = "-play mirror movie";
//видео по лабиринту
char* raspberryLabMovieCommand = "-play lab movie";

//видео по храму
char* raspberryTempleMovieCommand = "-play temple movie";

//команда включения питания манипулятору
char* raspberryHandOnCommand = "-on hand";

//команда соседней малине включить ультрафиолетовую лампу
char* raspberryUVLightOnCommand = "-on uvlight";

//команда малине, что колесница "доехала"
char* raspberryWheelchairCommand = "-wheelchair done";

//команда на отключение звука
char* raspberryStopSoundCommand = "-stop audio";
//команда на отключение видео
char* raspberryStopVideoCommand = "-stop video";

//аудио
//команда проигрывания звука лабиринта
char* raspberryMazeSoundCommand = "-play maze sound";

//команда проигрывания звука 4х кнопок
char* raspberryButtonsSoundCommand = "-play buttons sound";

//команда проигрывания звука сборки храма
char* raspberryTempleSoundCommand = "-play temple sound";

//неполная команда показа определённой картинки
char* raspberryShowImageCommand = "-image ";

char* raspberryStopImageCommand = "-stop image";

//количество цифр в коде
const unsigned short codeLength = 1;
//код для открытия двери в Древнюю Грецию
char correctCode[codeLength] = { '7'};

//указатель на текущую цифру кода
unsigned short codePointer = 0;
/*
 * Как это будет работать: при нажатии на кнопку проверяется, совпадает ли введённая цифра с той, что ожидается по указателю
 * Если совпадает - указатель инкрементируется, если нет - приравнивается к нуль (сбрасывается)
 * Если после ввода цифры указатель становится больше или равен размеру массива - значит, код ввели правильно и можно открывать дверь
 */

//реле на шилде инвертированы!
//реле, отвечающее за удержание колесницы Гелиоса
//по дефолту включено, выключается при подаче сигнала)
Relay relayWheelchair(WHEELCHAIR_RELAY_PIN, OFF, true);

//реле электрозамка на двери в Грецию
//по умолчанию включено, для открытия двери надо выключить
Relay relayGreeceDoor(GREECE_DOOR_RELAY_PIN, ON, true);

//реле подсветки лабиринта
Relay relayMaze(MAZE_RELAY_PIN, ON, true);

//реле светодиодов на двери в Грецию
//по умолчанию горит красный диод, реле выключено
//Relay doorLEDRelay(LED_RELAY_PIN, OFF, true);

//цифровая клавиатура
const byte ROWS = 4; 
const byte COLS = 4; 
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {2,3,4,5}; //connect to row pinouts 
byte colPins[COLS] = {6,7,8,9}; //connect to column pinouts

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//сервопривод с планкой, удерживающей шарик внутри стартовой позиции лабиринта
Servo mazeServo;

//обьявляем функцию для программного сброса Ардуины (на всякий случай)
void(* Reset) (void) = 0;//declare reset function at address 0

//костыльчик
//флаг того, что можно принимать код
bool codeAccept = false;

bool mirrorDone = false;


//отправляем команду на малину
void SendToRaspberry(char* message)
{
  Serial.println(message);
}

//отправляем команду на малину
void SendToRaspberry(char* message1, char* message2)
{
  Serial.print(message1);
  Serial.println(message2);
}

/*** КОМАНДЫ УПРАВЛЕНИЯ ***/

//функция, вызывающаяся тогда, когда в порту появляются данные
//так как тут у нас нет жёстких таймингов, управлять открытием-закрытием мы будем вручную, через Serial-команды
void serialEvent()
{
  int t = Serial.read();

  //на всякий случай - очистка буфера команд по знаку @
  if (t == 95) command = "";
  else  
  if (t != -1 && t!= 13 && t != 10)  
  {
    command += (char) t;
    //Serial.println(command);
  }
}

//функция, вызывающаяся тогда, когда в порту появляются данные
//при появлении стоп-символа тут же переправляем в основной порт
/*  В связи с внезапной необходимостью читать весь ввод с клавиатуры
 *  и отсутствием поддержки кириллицы ардуиной и терминалом малины
 *  часть символов будет кодироваться другими
 *  Ашопаделать?
 *  
 *  Список замен (11 символов)
 *  ж - #
 *  й - 1
 *  ч - 3
 *  ш - 4
 *  щ - 5
 *  ъ - 6
 *  ь - 8
 *  э - 9
 *  ю - 0
 *  я - *
 */
void serial1Event()
{
  if (Serial1.available())
  {
  int t = Serial1.read();
  
  //на всякий случай - очистка буфера команд по знаку _
  if (t == 95) command1 = "";
  else  
  if (t != -1 && t!= 13 && t != 10)  
  {
    //замена спецсимволов на обычные
    switch ( (char) t)
    {
      case '#':
        command1 += "zh";
        break;
      case '1':
        command1 += "ik";
        break;
      case '3':
        command1 += "ch";
        break;
      case '4':
        command1 += "sh";
        break;
      case '5':
        command1 += "sch";
        break;
      case '6':
        command1 += "tz";
        break;
      case '8':
        command1 += "mz";
        break;
      case '9':
        command1 += "ee";
        break;
      case '0':
        command1 += "yu";
        break;
      case '*':
        command1 += "ya";
        break;
      default:
        command1 += (char) t;
        break;
    }
  }
  else
  {
    char* tempBuf = new char[command1.length()+1];

    SendToRaspberry(raspberryStopImageCommand);
    delay(100);
    
    command1.toCharArray(tempBuf, command1.length()+1);
    
    SendToRaspberry(raspberryShowImageCommand,tempBuf);
    
  if (strcmp(tempBuf, "zerkalo") == 0) 
    mirrorDone = true;
  
    delete tempBuf;
  }
  }
}

//проверка ввода корректного кода для открытия двери в следующую комнату (Древняя Греция)
bool EnterCode(char key)
{
  bool result = false;
    //проверяем, совпадает ли нажатая клавиша с ожидаемой
    if (key == correctCode[codePointer])
    {
      codePointer++;
      //введён ли код до конца?
      if (codePointer >= codeLength)
      {
    result = true;
      }
    }
    //дети где-то ошиблись
    else 
    {
      //сбрасываем указатель на начало кода
      codePointer = 0;
    }
  return result;
}



//функция парсинга входящих команд (UART)
void ParceInput()
{
  //команда перезагрузки ардуины. Пользоваться с осторожностью!
  if ( command == "reset" )
    {
      command = "";
      Reset();
      return;
    }
  if (command == "greece code allow" )
  {
    command = "";
    codeAccept = true;
    return;
  }
  /*
   * Аварийный блок команд
   */

   //колесница
   if ( command == "wheelchair" )
    {
      command = "";
     relayWheelchair.Switch();
      return;
    }

    //дверь Греция
    if ( command == "door" )
    {
      command = "";
     relayGreeceDoor.Switch();
      return;
    }

    //подсветка лабиринта
    if ( command == "maze light" )
    {
      command = "";
     relayMaze.Switch();
      return;
    }

    //"прохождение" зеркала
    if ( command == "mirror" )
    {
      command = "";
      mirrorDone = true;
      return;
    }
    
    //открыть серво лабиринта
    if ( command == "maze open" )
    {
      command = "";
      //открываем серву
       for(int pos = 0; pos <= 180; pos += 1) // goes from 0 degrees to 180 degrees 
      {                                  // in steps of 1 degree 
        mazeServo.write(pos);              // tell servo to go to position in variable 'pos' 
        delay(15);                       // waits 15ms for the servo to reach the position 
      } 
      
      return;
    }
    
    //закрыть серво лабиринта
    if ( command == "maze close" )
    {
      command = "";
      //закрываем серву
      for(int pos = 180; pos >= 80; pos -= 1) // goes from 0 degrees to 180 degrees 
      {                                  // in steps of 1 degree 
        mazeServo.write(pos);              // tell servo to go to position in variable 'pos' 
        delay(15);                       // waits 15ms for the servo to reach the position 
      } 
      return;
    }

    //СИД на двери
    if ( command == "led" )
    {
      command = "";
     //doorLEDRelay.Switch();
      return;
    }
}

/***  КОД ФАЗ КВЕСТА  ***/

/* Фаза 1.
 * Начинается с ввода кода замка на двери в Грецию, заканчивается открытием этого самого замка
*/
void Phase1()
{
    //флаг окончания фазы
  bool phaseCompleted = false;

  command="";
  
  //основной цикл фазы
  while (!phaseCompleted)
  {
    serialEvent();

    if (codeAccept)
    {
    char key = keypad.getKey();

    if (key != NO_KEY){
      //если ввели корректный код - то вернётся true
      if ( EnterCode(key) )
      {
        //открываем дверь сюда
        relayGreeceDoor.Off();
        
        //завершаем фазу
        phaseCompleted = true;
      }
    }
    }
    //если на вход что-то поступило - обрабатываем
    if ( command != "" )
    {
      ParceInput();
    }
  }
}


/* Фаза 2.
 * Начинается с получения сигнала с датчика движения
 * Далее - проигрывание ролика на малине
 * Заканчивается после получения от ардуины-"зеркала" информации о корректном вводе кода
*/
void Phase2()
{
  //флаг окончания фазы
  bool phaseCompleted = false;
  

  motionSensorEvent = true;
  mirrorDone = false;

  Serial1.flush();
 
  while (!phaseCompleted)
  {  
    serialEvent();
    
    //обработка команд с ардуины-"зеркала"
    serial1Event();
    
    //есть сигнал с датчика движения
    if (digitalRead(MOTION_SENSOR_PIN) == HIGH && motionSensorEvent)
    {
      //опускаем флаг, чтобы больше сюда не заходить
      motionSensorEvent = false;

      //задержка перед видео
      delay(5000);
      
      //посылаем на малину команду запуска ролика
      SendToRaspberry(raspberryMirrorMovieCommand);

     //задержка на видео
     for (int i = 0; i < 44; i++) 
     {
      delay(1000);
     }
    }
    
    //ожидаем сигнал от "зеркала"
    if (mirrorDone)
    {

      //ждём чуток
      delay(1000);

      SendToRaspberry(raspberryStopImageCommand);

      delay(500);

      //запускаем видео о минотавре
      SendToRaspberry(raspberryLabMovieCommand);

      //ждём
      for (int i = 0; i < 8; i++)
      {
        relayMaze.Switch();
        delay(1000);
      }
      
      //а ещё тут надо поднять серву, чтобы освободить шарик
  for(int pos = 0; pos <= 180; pos += 1) // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    mazeServo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
      
      phaseCompleted = true;
    }
    
    //если на вход что-то поступило - обрабатываем
    if ( command != "" )
    {
      ParceInput();
    }

      //если на вход от зеркала что-то поступило - обрабатываем
    if ( command1 != "" )
    {
      ParceInput();
    }
  }
}

/* Фаза 3.
 * Начинается после старта манипулятора
 * В программе: 
 * ожидание  прохождения детьми лабиринта
 * включение ультрафиолетовой лампы
 * нажимание четырёх кнопок
 * включение манипулятора
 * сборка храма
 * старт колесницы
 * открытие ящика с шестерёнкой (открывается с соседней ардуины после команды на "я всё"
 * Заканчивается после отпускания колесницы.
*/
void Phase3()
{
  //флаг окончания фазы
  bool phaseCompleted = false;

  buttonTempleConstructionEvent = true;

  bool mazeCompleted = false;
  bool fourButtonsPressed = false;
  
  while (!phaseCompleted)
  {
    serialEvent();

    //ждём пока дети пройдут лабиринт
    if (digitalRead(MAZE_BUTTON_PIN) == LOW && !mazeCompleted)
    {
      delay(500);
      if (digitalRead(MAZE_BUTTON_PIN) == LOW)
      {
        //играем звук лабиринта
        SendToRaspberry(raspberryMazeSoundCommand);
        
      //попросим соседа включить лампу
      SendToRaspberry(raspberryUVLightOnCommand);

      delay(1000);

      SendToRaspberry(raspberryStopImageCommand);
      delay(1000);

      //проиграем видео о храме
      SendToRaspberry(raspberryTempleMovieCommand);

      //выходим из блока кода и делаем всё, чтобы сюда не вернуться
      mazeCompleted = true;
      }
    }

    //ждём нажатия четырёх кнопок одновременно
    if (digitalRead(FOUR_BUTTONS_PIN) == LOW && !fourButtonsPressed && mazeCompleted)
    {
      delay(500);
      if (digitalRead(FOUR_BUTTONS_PIN) == LOW)
      {
      //играем звук 4х кнопок
      SendToRaspberry(raspberryButtonsSoundCommand);
      
      //включаем манипулятор
      SendToRaspberry(raspberryHandOnCommand);

      fourButtonsPressed = true;

      delay(1000);
    }
    }

    //и ждём постройки храма
    if (digitalRead(TEMPLE_PIN) == LOW && fourButtonsPressed)
    {
      delay(500);
      if (digitalRead(TEMPLE_PIN) == LOW)
      {
      //играем звук сборки храма
      SendToRaspberry(raspberryTempleSoundCommand);
      
      //и отпускаем колесницу в свободный полёт
      relayWheelchair.Switch();
      
      //и завершаем фазу
      phaseCompleted = true;

      //и костыльно ждём, пока колесница не доедет до конца своего пути
      delay(6000);

      //и продолжаем квест
      SendToRaspberry(raspberryWheelchairCommand);
    }   
    }

    //если на вход что-то поступило - обрабатываем
    if ( command != "" )
    {
      ParceInput();
    }
  }
}

/*** ПЕРЕМЕННЫЕ ***/

void setup() {
	//======================================================
	/***	ПЕРВОНАЧАЛЬНАЯ НАСТРОЙКА ПИНОВ СИСТЕМЫ И ПРЕРЫВАНИЙ	***/

  Serial.begin(9600);

  //запускаем второй экземпляр для работы со второй ардуиной
  Serial1.begin(9600);
  
  //греческий храм - по сути одна линия, которая будет куда-то замыкаться. Абстрактно - то же прерывание по получению/удалению сигнала
  pinMode(TEMPLE_PIN, INPUT);
  digitalWrite(TEMPLE_PIN, HIGH);

  //настраиваемся на сигнал от ардуины, заведующей "Зеркалом"
  pinMode(MIRROR_PIN, INPUT);

  //настраиваемся на сигнал от датчика движения
  pinMode(MOTION_SENSOR_PIN, INPUT);
  digitalWrite(MOTION_SENSOR_PIN, HIGH);
  
  //настраиваемся на кнопку-микровыключатель в конце лабиринта, которую нажмёт выкатившийся шарик
  pinMode(MAZE_BUTTON_PIN, INPUT);
  digitalWrite(MAZE_BUTTON_PIN, HIGH);

  //настраиваемся на четыре-кнопки
  pinMode(FOUR_BUTTONS_PIN, INPUT);
  digitalWrite(FOUR_BUTTONS_PIN, HIGH);
   
  //исключаем случайные нажатия на кнопки клавиатуры
  //порог - 50 миллисекунд
  keypad.setDebounceTime(50);

  //инициализируем сервопривод
  mazeServo.attach(SERVO_PIN);
  
  //даём время датчику движения после перезагрузки на калибровку
  //примерно 30 секунд
  int calibrationTime = 10;  //10
  for(int i = 0; i < calibrationTime; i++)
  {
    //Во время калибровки будет мигать сигнальный диод
    i % 2 ? digitalWrite(ledPin, HIGH) : digitalWrite(ledPin, LOW);
    delay(1000);
  }

    //очищаем буфер от мусора
  while (Serial.available() > 0) {
                // read the incoming byte:
                int incomingByte = Serial.read();
  }
  
  /***	КОНЕЦ ПРЕДВАРИТЕЛЬНОЙ НАСТРОЙКИ СИСТЕМЫ	***/
  
  /***	ПООЧЕРЁДНЫЙ ЗАПУСК ФАЗ КВЕСТА	***/
  
  Phase1();
  Phase2();
  Phase3();
  
  /***	КОНЕЦ ФАЗ КВЕСТА	***/
  
}



/***	КОНЕЦ КОДА ФАЗ КВЕСТА	***/

void loop() {
  
  //если на вход что-то поступило - обрабатываем
  if ( command != "" )
  {
    ParceInput();
  }

}

