/* Скетч предназначен для реал-квеста "Машина времени"
 *  
 *  Данный скетч покрывает Лабораторию 3 (Рим)
 * 
 *	Version 2.1
 *
 *	По большому количеству здравых размышлений понял, что старый метод решения проблемы "в лоб" некорректен.
 *	Решил сделать так: вместо мельтешения флагов я поделил последовательность квестовых действий на фазы.
 *	Фазы будут вызываться последовательно внутри setup(). Это гарантирует, что каждая фаза наступит только один раз.
 *	Внутри фазы будет что-то вроде while (!phaseCompleted) {} с обязательным гашением флага после конца фазы.
 *	После прохода всех-всех-всех фаз на все ардуины пройдёт команда Reset(), что вернёт их в стартовую конфигурацию.
 *	Внутри каждой фазы будет находиться блок чтения команд из UART для того, чтобы администратор мог вмешаться в любую из них, если дети попадут в трудное положение. 
 */

//библиотека работы с реле
#include <Relay.h>

//библиотека для использования прерываний на произвольных пинах Ардуины
#include <PinChangeInt.h>

/***ЗНАЧЕНИЯ ПИНОВ***/
//пин кнопки "Да"
#define YES_BUTTON_PIN 11
//пин кнопки "Нет"
#define NO_BUTTON_PIN 12

//пин Пизанской башни
#define TOWER_PIN 9

//пин микровыключателя на двери
#define DOOR_BUTTON_PIN 10

//пин микровыключателя в ящике
#define BOX_BUTTON_PIN 8

//пины УЗ-дальномера в башне
#define TOWER_ECHO_PIN 3
#define TOWER_TRIG_PIN 2
#define TOWER_VCC_PIN 4

/********************РЕЛЕ***********************/
//блоки реле
//пин реле электрозамка на двери Рима
#define ROME_DOOR_RELAY_PIN A2

//пин реле ящика с шестерёнкой в Греции
#define GREECE_BOX_RELAY_PIN A1

//пин реле светодиодов на двери
#define ROME_LED_RELAY_PIN A0

//второй блок (силовой)
//пин реле для манипулятора
#define HAND_RELAY_PIN  A4
//пин реле ультрафиолетовой лампы
#define LAMP_RELAY_PIN  A3


//===============================================

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/
//флаг срабатывания события "Собрана Пизанская башня"
volatile bool TowerCompletedEvent = false;

//задание Императора с вопросами
//кнопка "Да"
volatile bool buttonYesPressedEvent = false;

//кнопка "Нет"
volatile bool buttonNoPressedEvent = false;

//админ-флаг, отключающий необходимость прохождения башни
bool towerDone = false;

/*** ФЛАГИ ПРЕРЫВАНИЙ ***/

/*** ПЕРЕМЕННЫЕ ***/

String command = "";

//команды для малины
char* raspberryPlayImperorCallingMovieCommand = "-play imperor calling movie";

//команда для начала цикла вопросов
char* raspberryPlayImperorStartQuestionCommand = "-play imperor start question";

//команды для запуска серии видео с вопросами
char* raspberryPlayVideoQuestion1 = "-play question one";
char* raspberryPlayVideoQuestion2 = "-play question two";
char* raspberryPlayVideoQuestion3 = "-play question three";
char* raspberryPlayVideoQuestion4 = "-play question four";
char* raspberryPlayVideoQuestion5 = "-play question five";
char* raspberryPlayVideoQuestion6 = "-play question six";
char* raspberryPlayVideoQuestion7 = "-play question seven";
char* raspberryPlayVideoQuestion8 = "-play question eight";
char* raspberryPlayVideoQuestion9 = "-play question nine";

int questionArrayLenght = 9;
//массив с корректными ответами (1 - да, 0 - нет)
bool correctAnswers[] = { 1,1,0,0,1,0,1,0,0};

//массив указателей на фоманды для проигрывания видеовопросов
char* questionCommand[] = { raspberryPlayVideoQuestion1,
                            raspberryPlayVideoQuestion2,
                            raspberryPlayVideoQuestion3,
                            raspberryPlayVideoQuestion4,
                            raspberryPlayVideoQuestion5,
                            raspberryPlayVideoQuestion6,
                            raspberryPlayVideoQuestion7,
                            raspberryPlayVideoQuestion8,
                            raspberryPlayVideoQuestion9};

//длительности видео (чтобы не включать прерывания раньше необходимого) в секундах
int questionVideoDelays[] = {10,11,12,12,9,11,11,11,10};

//команды проигрывания видео на верные ответы
char* raspberryPlayVideoCorrectAnswer1 = "-play correct one";
char* raspberryPlayVideoCorrectAnswer2 = "-play correct two";
char* raspberryPlayVideoCorrectAnswer3 = "-play correct three";
char* raspberryPlayVideoCorrectAnswer4 = "-play correct four";
char* raspberryPlayVideoCorrectAnswer5 = "-play correct five";
char* raspberryPlayVideoCorrectAnswer6 = "-play correct six";
char* raspberryPlayVideoCorrectAnswer7 = "-play correct seven";
char* raspberryPlayVideoCorrectAnswer8 = "-play correct eight";
char* raspberryPlayVideoCorrectAnswer9 = "-play correct nine";

//массив вышеперечисленного
char* correctAnswer[] = {raspberryPlayVideoCorrectAnswer1,
                          raspberryPlayVideoCorrectAnswer2,
                          raspberryPlayVideoCorrectAnswer3,
                          raspberryPlayVideoCorrectAnswer4,
                          raspberryPlayVideoCorrectAnswer5,
                          raspberryPlayVideoCorrectAnswer6,
                          raspberryPlayVideoCorrectAnswer7,
                          raspberryPlayVideoCorrectAnswer8,
                          raspberryPlayVideoCorrectAnswer9};

//длительности видео (чтобы не включать прерывания раньше необходимого) в секундах
int rightAnswersVideoDelays[] = {5,5,7,5,7,5,5,8,6};

//команды проигрывания видео на неверные ответы
char* raspberryPlayVideoWrongAnswer1 = "-play wrong one";
char* raspberryPlayVideoWrongAnswer2 = "-play wrong two";
char* raspberryPlayVideoWrongAnswer3 = "-play wrong three";
char* raspberryPlayVideoWrongAnswer4 = "-play wrong four";
char* raspberryPlayVideoWrongAnswer5 = "-play wrong five";
char* raspberryPlayVideoWrongAnswer6 = "-play wrong six";
char* raspberryPlayVideoWrongAnswer7 = "-play wrong seven";
char* raspberryPlayVideoWrongAnswer8 = "-play wrong eight";
char* raspberryPlayVideoWrongAnswer9 = "-play wrong nine";

//массив вышеперечисленного
char* wrongAnswer[] = {raspberryPlayVideoWrongAnswer1,
                        raspberryPlayVideoWrongAnswer2,
                        raspberryPlayVideoWrongAnswer3,
                        raspberryPlayVideoWrongAnswer4,
                        raspberryPlayVideoWrongAnswer5,
                        raspberryPlayVideoWrongAnswer6,
                        raspberryPlayVideoWrongAnswer7,
                        raspberryPlayVideoWrongAnswer8,
                        raspberryPlayVideoWrongAnswer9};

//длительности видео (чтобы не включать прерывания раньше необходимого) в секундах
int wrongAnswersVideoDelays[] = {6,6,6,8,8,8,7,8,5};

//номер текущего вопроса (для ориентирования в массиве)
int currentQuestion = 0;

//инициализация счётчика правильных ответов
//если количество правильных ответов 4 и больше - проигрывается одно видео, иначе - другое
int rightAnswersCount = 0;

//видео с поздравлением от Императора
char* raspberryPlayGoodEmperorVideo = "-play good emperor";

//видео с проклятием от Императора
char* raspberryPlayBadEmperorVideo = "-play bad emperor";

//видео с последним учёным
char* raspberryPlayScientistRomeVideo = "-play scientist rome";

//аудио
//звук открытия двери
char* raspberryOpenDoorSoundCommand = "-play open door sound";
//звук правильного ответа
char* raspberryCorrectSoundCommand = "-play correct sound";
//звук неправильного ответа
char* raspberryWrongSoundCommand = "-play wrong sound";
//звук собранной башни
char* raspberryTowerSoundCommand = "-play tower sound";

//команда на первую малину
char* raspberryAllDoneCommand = "-all done";

//команда на отключение звука
char* raspberryStopSoundCommand = "-stop audio";
//команда на отключение видео
char* raspberryStopVideoCommand = "-stop video";

//реле, контролирующее доступ к Риму
//открыто без питания, в начале квеста закрыто
Relay doorRomeRelay(ROME_DOOR_RELAY_PIN,ON, true);

//реле светодиодов на двери
Relay doorLEDRelay(ROME_LED_RELAY_PIN, OFF, true);

//реле ультрафиолетовой лампы
Relay UVLampRelay(LAMP_RELAY_PIN, OFF, true);

//реле включения манипулятора
Relay handRelay(HAND_RELAY_PIN, ON, true);

//реле ящичка с шестерёнкой в Греции
//просто сюда оно ближе
Relay boxRelay(GREECE_BOX_RELAY_PIN, OFF, true);

/*** ПЕРЕМЕННЫЕ ***/

//обьявляем функцию для программного сброса Ардуины (на всякий случай)
void(* Reset) (void) = 0;//declare reset function at address 0

void setup() {

	//======================================================
	/***	ПЕРВОНАЧАЛЬНАЯ НАСТРОЙКА ПИНОВ СИСТЕМЫ И ПРЕРЫВАНИЙ	***/
	
	//инициализируем UART, через него мы получим информацию о том, что дверь можно открыть
	Serial.begin(9600);
	
	//выставляем порты
	
	//кнопка "Да"
	pinMode(YES_BUTTON_PIN, INPUT);
	//включаем внутренний подтягивающий резистор
	digitalWrite(YES_BUTTON_PIN, HIGH);
	
  //кнопка "Нет"
  pinMode(NO_BUTTON_PIN, INPUT);
  //включаем внутренний подтягивающий резистор
  digitalWrite(NO_BUTTON_PIN, HIGH);

 pinMode(TOWER_VCC_PIN, OUTPUT);
 digitalWrite(TOWER_VCC_PIN, HIGH);

 //настраиваем пины для УЗ в башне
 pinMode(TOWER_TRIG_PIN, OUTPUT);
 digitalWrite(TOWER_TRIG_PIN, LOW);

 pinMode(TOWER_ECHO_PIN, INPUT);

  //кнопка внутри ящика с шестерёнкой
  pinMode(BOX_BUTTON_PIN, INPUT);
  digitalWrite(BOX_BUTTON_PIN, HIGH);

    //очищаем буфер от мусора
  while (Serial.available() > 0) {
                // read the incoming byte:
                int incomingByte = Serial.read();
  }
  
  /***	ПООЧЕРЁДНЫЙ ЗАПУСК ФАЗ КВЕСТА	***/
  
  Phase1();
  Phase2();
  Phase3();
  Phase4();
  
  /***	КОНЕЦ ФАЗ КВЕСТА	***/
}

/***	КОД ФАЗ КВЕСТА	***/

/* Фаза 1.
 * Начинается с получения сигнала от кнопки на двери (микровыключатель)
 * Далее играет ролик "Ииператор зовёт"
 * И мы открываем дверь в Рим
*/

//флаг того, что сюда уже можно, прилетает от соседа
bool allowed = false;

void Phase1()
{
	//флаг конца фазы
	bool phaseCompleted = false;

  command = "";  
	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();
		//ждём получения сигнала, что дверь открыта
		if (allowed)
		{
    delay(17000);
			//посылаем команду в малину
			SendToRaspberry(raspberryPlayImperorCallingMovieCommand);

    for (int i =0; i<10;i++) delay(1000);

			//завершаем фазу
			phaseCompleted = true;
		}
		
		if ( command != "" )
		{
			ParceInput();
		}
	}
}

/* Фаза 2.
 * Начинается с ожидания сборки башни
 * Для контроля процесса добавлен флаг towerDone, который можно поднять из админ-интерфейса для помощи детям
 */
 
void Phase2()
{
	//флаг конца фазы
	bool phaseCompleted = false;

  //основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();
		//ждём сборки Колизея

   int duration, cm; 
    digitalWrite(TOWER_TRIG_PIN, LOW); 
    delayMicroseconds(2); 
    digitalWrite(TOWER_TRIG_PIN, HIGH); 
    delayMicroseconds(10); 
    digitalWrite(TOWER_TRIG_PIN, LOW); 
    duration = pulseIn(TOWER_ECHO_PIN, HIGH); 
    cm = duration / 58;
    delay(100);

    //эмпирические показания
		if ( (cm < 28 && cm > 23) || towerDone)
		{
      //играем звук сборки башни
      SendToRaspberry(raspberryTowerSoundCommand);
      delay(5000);
			//Император готов задавать вопросы
      SendToRaspberry(raspberryPlayImperorStartQuestionCommand);

      //ждём, сколько надо
      delay(44000);
      
			phaseCompleted = true;
	  }
		
		
		if ( command != "" )
		{
			ParceInput();
		}
	}
}

/* Фаза 3.
 * Здесь Император задаёт вопросы
*/
void Phase3()
{
	//флаг конца фазы
	bool phaseCompleted = false;

  //подключаем прерывания к кнопкам (раньше они не были нужны)
  
  //срабатывание по спадающему фронту, т.е. по замыканию на землю
  attachPinChangeInterrupt(NO_BUTTON_PIN,ButtonNoPressedEventHandler,FALLING);

  //срабатывание по спадающему фронту, т.е. по замыканию на землю
  attachPinChangeInterrupt(YES_BUTTON_PIN,ButtonYesPressedEventHandler,FALLING);

  //флаг того, что вопрос задан и ожидается ответ
  bool nextQuestion = true;
  
	//основной цикл фазы
	while (!phaseCompleted)
	{
    serialEvent();
    
    //пора задавать новый вопрос
    if (nextQuestion)
    {
      //обнуляем ответы
      buttonYesPressedEvent = false;
      buttonNoPressedEvent = false;

      //посылаем в малину запрос на следующее видео
      SendToRaspberry(questionCommand[currentQuestion]);

      //ждём, пока оно проиграется
      delay(questionVideoDelays[currentQuestion] * 1000);

      //срабатывание по спадающему фронту, т.е. по замыканию на землю
      attachPinChangeInterrupt(NO_BUTTON_PIN,ButtonNoPressedEventHandler,FALLING);

      //срабатывание по спадающему фронту, т.е. по замыканию на землю
      attachPinChangeInterrupt(YES_BUTTON_PIN,ButtonYesPressedEventHandler,FALLING);
      
      //опускаем флаг - ждём ответа
      nextQuestion = false;
    }
    //если нет - просто ждём ответа на предыдущий
    else
    {
      //если нажата одна из кнопок
      if (buttonYesPressedEvent || buttonNoPressedEvent)
      {
        //для начала проверим, какая именно кнопка нажата
        bool answer = false;
        if (buttonYesPressedEvent) answer = true;

        //обрубаем на всякий случай видео, если оно идёт
        SendToRaspberry(raspberryStopVideoCommand);
        delay(500);
      
        //проверяем валидность ответа
        //если он правильный
        if (answer == correctAnswers[currentQuestion])
        {
          //засчитываем правильный ответ
          rightAnswersCount++;

          //проигрываем звук правильного ответа
          SendToRaspberry(raspberryCorrectSoundCommand);
          delay(2000);

          //произрываем видеопоздравление
          SendToRaspberry(correctAnswer[currentQuestion]);

          //ждём, пока оно доиграет
          delay(rightAnswersVideoDelays[currentQuestion] * 1000);
        }
        //и если неправильный
        else
        {

          //проигрываем звук неправильного ответа
          SendToRaspberry(raspberryWrongSoundCommand);
          delay(2000);
          
          //проигрываем видеопроклятие
          SendToRaspberry(wrongAnswer[currentQuestion]);

          //ждём, пока оно доиграет
          delay(wrongAnswersVideoDelays[currentQuestion] * 1000);
        }

        //поднимаем флаг следующего вопроса или выходим из фазы
        if (currentQuestion < 8) 
        {
          nextQuestion = true;
          currentQuestion++;
        }
        else
        {
          phaseCompleted = true;
        }
        
        
      }
    }
	}
}

void Phase4()
{
  bool phaseCompleted = false;


    //все вопросы заданы, нужен вердикт
    //если был хоть один верный ответ
    if (rightAnswersCount > 0)
    {
      //Ымператор поздравляе!
      SendToRaspberry(raspberryPlayGoodEmperorVideo);
      //задержка на воспроизведение видео
      delay(8000);
    }
    //если дети дали неправильные ответы на половину вопросов или больше
    else
    {
      //Ымператор ругаеццо
      SendToRaspberry(raspberryPlayBadEmperorVideo);
      //задержка
      delay(12000);
    }

  //теперь ждём, пока дети откроют ящик ключом, получаем оттуда инфо
  //играем ролик с ученым и посылаем в малину запрос на общение со стартовой малиной

  while (!phaseCompleted)
  {
    serialEvent();
    //ящик таки открыли
    if (digitalRead(BOX_BUTTON_PIN) == HIGH)
    {
      //проигрываем видео
      SendToRaspberry(raspberryPlayScientistRomeVideo);

      SendToRaspberry(raspberryAllDoneCommand);

      phaseCompleted = true;
    }
  }
}
/***	КОНЕЦ КОДА ФАЗ КВЕСТА	***/

void loop() {
  
  //если на вход что-то поступило - обрабатываем
  if ( command != "" )
  {
    ParceInput();
  }

}

/*** КОМАНДЫ УПРАВЛЕНИЯ ***/

//функция, вызывающаяся тогда, когда в порту появляются данные
//так как тут у нас нет эёстких таймингов, управлять открытием-закрытием мы будем вручную, через Serial-команды
void serialEvent()
{
  int t = Serial.read();

  //на всякий случай - очистка буфера команд по знаку @
  if (t == 95) command = "";
  else
  if (t != -1 && t!= 13 && t != 10)  
  {
    command += (char) t;
    //Serial.println(command);
  }
}

//функция парсинга входящих команд (UART)
void ParceInput()
{
	//команда перезагрузки ардуины. Пользоваться с осторожностью!
	if ( command == "reset" )
    {
      command = "";
	    Reset();
      return;
    }

   if (command == "wheelchair done" )
   {
      command = "";
      //открываем дверь в Рим
      doorRomeRelay.Switch();

      doorLEDRelay.Switch();

      //играем звук открытия двери
      SendToRaspberry(raspberryOpenDoorSoundCommand);
    
      //отдаём шестерёнку
      boxRelay.Switch();

      allowed = true;
   }

    //также годятся как аварийные команды
   if (command == "on hand")
   {
    command = "";
    //включаем подсветку манипулятора
    handRelay.Switch();
    
   }

   if (command == "on uvlight")
   {
    command = "";
    //включаем ультрафиолетовую лампу
    UVLampRelay.Switch();
   }

  /*
   * Блок аварийных команд
   */

   //открыть дверь в Рим
   if ( command == "door" )
    {
      command = "";
     doorRomeRelay.Switch();
      return;
    }

    //светодиоды на двери
    if ( command == "led" )
    {
      command = "";
     doorLEDRelay.Switch();
      return;
    }

    //ящик с шестерёнкой
    if ( command == "box" )
    {
      command = "";
     boxRelay.Switch();
      return;
    }

    if ( command == "tower done" )
    {
      command = "";
      towerDone = true;
      return;
    }
}

//отправляем команду на малину
void SendToRaspberry(char* message)
{
  Serial.println(message);
}

/***	ОБРАБОТЧИКИ ПРЕРЫВАНИЙ	***/

//прерывание с кнопки "Да"
void ButtonYesPressedEventHandler()
{
	buttonYesPressedEvent = true;

  //нам нужно только одно срабатывание, так что отключаем прерывания
  detachPinChangeInterrupt(YES_BUTTON_PIN);
  detachPinChangeInterrupt(NO_BUTTON_PIN);
}

//прерывание с кнопки "Нет"
void ButtonNoPressedEventHandler()
{
	buttonNoPressedEvent = true;
  
  //нам нужно только одно срабатывание, так что отключаем прерывания
  detachPinChangeInterrupt(YES_BUTTON_PIN);
  detachPinChangeInterrupt(NO_BUTTON_PIN);
}


/***	КОНЕЦ ОБРАБОТЧИКОВ ПРЕРЫВАНИЙ	***/
